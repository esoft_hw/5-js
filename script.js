//______________________
//
// ВВОД ДАННЫХ В МАТРИЦУ
// Крестики - англ. x
// Нолики - англ. o
// Пустая клетка - null
//______________________

// функция проверки
function checkField(field) {

    for(let i = 0; i < 3; i++)
    {
        if(field[0][i] === field[1][i] && field[1][i] === field [2][i] && field[1][i] !== null)
            return field[0][i];
        
        else if(field[i][0] === field[i][1] && field[i][1] === field [i][2] && field[i][1] !== null)
            return field[i][0];

        else if(field[0][0] === field[1][1] && field[1][1] === field [2][2] && field[1][1] !== null)
            return field[1][1];

        else if(field[0][2] === field[1][1] && field[1][1] === field [2][0] && field[1][1] !== null)
            return field[1][1];
    }

    return '-';
};

// функция вывода
function printResult(value) {
    
    if(value === 'x')
        alert('Крестики победили');

    else if(value === 'o')
        alert('Нолики победили');
    
    else if(value === '-')
        alert('Ничья');
}

// ИТОГОВАЯ Функция
function findWiner(gameField) {
    printResult(checkField(gameField));
}

// TESTS
const gameField1 = [
    ['o', 'o', 'o'],
    ['x', null, 'x'],
    ['o', 'x', 'x']
]

const gameField2 = [
    ['o', 'x', 'o'],
    ['x', 'o', 'x'],
    ['o', 'x', 'x']
]

const gameField3 = [
    ['o', null, 'o'],
    ['x', null, 'x'],
    ['o', null, 'x']
]

const gameField4 = [
    ['o', 'o', null],
    ['x', 'x', 'x'],
    ['o', 'x', 'x']
]

const gameField5 = [
    [null, 'o', 'o'],
    ['x', null, 'x'],
    ['o', 'x', 'x']
]

findWiner(gameField1);
findWiner(gameField2);
findWiner(gameField3);
findWiner(gameField4);
findWiner(gameField5);
